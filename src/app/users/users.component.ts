import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

    users$: Object;

    constructor(private data: DataService) { }

    ngOnInit() {
        this.data.getUsers().subscribe(
            data => this.users$ = data
        )
    }

    searchPerson(term: string, item: Object) {
        term = term.toLocaleLowerCase();
        return item['name'].toLocaleLowerCase().indexOf(term) !== -1 || item['phone'].toLocaleLowerCase().indexOf(term) !== -1;
    }
}
